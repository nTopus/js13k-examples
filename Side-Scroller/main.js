const centerStrip = document.getElementById('center')
const centerStripRect = centerStrip.getBoundingClientRect()

const BASE_HSPEED = 3

const [minWidth, maxWidth] = [50, 150]
const [minHeight, maxHeight] = [75, 175]

function createPlayer() {
  const div = document.createElement('div')
  div.setAttribute('id', 'player')
  div.verticalSpeed = 0
  div.horizontalSpeed = BASE_HSPEED
  div.style.top = '32px'
  div.style.left = '240px'
  centerStrip.appendChild(div)
  return div
}

function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min
}

function rect(position = 'top') {
  const div = document.createElement('div')
  const height = rand(minHeight, maxHeight);
  const width = rand(minWidth, maxWidth)
  div.style.height = `${height}px`
  div.style.width = `${width}px`
  div.style.backgroundColor = 'black'
  div.style.position = 'absolute'
  div.style[position] = '0px'
  div.style.right = `-${width}px`
  centerStrip.appendChild(div)
  return div
}

const topRects = [rect()]
const bottomRects = [rect('bottom')]

function moveRects(rectList, position = 'top', amount) {
  for (const rect of rectList) {
    const currentPosition = parseFloat(rect.style.right)
    rect.style.right = `${currentPosition + amount}px`
  }

  const leftmostRect = rectList.at(0)
  const leftmostRight = parseFloat(leftmostRect.style.right)
  if (leftmostRight >= centerStripRect.width) {
    rectList.shift().remove()
  }

  const rightmostRect = rectList.at(-1)
  const rightmostRight = parseFloat(rightmostRect.style.right)
  if (rightmostRight >= 100) {
    rectList.push(rect(position))
  }
}

const player = createPlayer()

function applyVerticalMovement(object, movement) {
  const currentY = parseFloat(object.style.top)
  object.style.top = `${currentY + movement}px`
}

function applyVerticalSpeed(object, dt) {
  if ('verticalSpeed' in object) {
    applyVerticalMovement(object,  object.verticalSpeed * dt)
  }
}

function applyVerticalAcceleration(object, acceleration, dt) {
  if ('verticalSpeed' in object) {
    const speed = object.verticalSpeed
    object.verticalSpeed = speed + acceleration * dt
  }
}

function normalizeHorizontalSpeed(object, dt) {
  if ('horizontalSpeed' in object) {
    const speed = object.horizontalSpeed
    if (speed < BASE_HSPEED) {
      const ds = BASE_HSPEED - speed
      object.horizontalSpeed = Math.min(speed + (ds / 50) * dt, BASE_HSPEED)
    }
  }
}

let isJumping = false
let hasJumped = false

document.addEventListener('keydown', (e) => {
  if (e.key === ' ' || e.key === 'Spacebar') {
    isJumping = true
  }
})
document.addEventListener('keyup', (e) => {
  if (e.key === ' ' || e.key === 'Spacebar') {
    isJumping = false
  }
})

function objectCollider(a, b, areColliding, collisionEffect) {
  if (areColliding(a, b)) {
    collisionEffect(a, b)
  }
}

function aabbCollisionCheck(a, b) {
  const rectA = a.getBoundingClientRect()
  const rectB = b.getBoundingClientRect()
  const dx = (rectA.x + rectA.width / 2) - (rectB.x + rectB.width / 2)
  const dy = (rectA.y + rectA.height / 2) - (rectB.y + rectB.height / 2)
  const sumHalfWidth = rectA.width / 2 + rectB.width / 2
  const sumHalfHeight = rectA.height / 2 + rectB.height / 2
  return !(Math.abs(dx) >= sumHalfWidth || Math.abs(dy) >= sumHalfHeight)
}

function paintObject(_a, b) {
  b.style.backgroundColor = 'red'
}

function stopPlayer(a, b) {
  a.verticalSpeed = -a.verticalSpeed
  a.horizontalSpeed = -a.horizontalSpeed
}

function separate(a, b) {
  const rectA = a.getBoundingClientRect()
  const rectB = b.getBoundingClientRect()
  const dx = (rectA.x + rectA.width / 2) - (rectB.x + rectB.width / 2)
  const dy = (rectA.y + rectA.height / 2) - (rectB.y + rectB.height / 2)
  const sumHalfWidth = rectA.width / 2 + rectB.width / 2
  const sumHalfHeight = rectA.height / 2 + rectB.height / 2
  let sx = sumHalfWidth - Math.abs(dx)
  let sy = sumHalfHeight - Math.abs(dy)
  if (sx < sy) {
    if (sx > 0) {
      sy = 0
    }
  }
  else if (sy > 0) {
    sx = 0
  }
  if (dx < 0) {
    sx = -sx
  }
  if (dy < 0) {
    sy = -sy
  }
  applyVerticalMovement(player, sy)
  moveRects(topRects, 'top', sx)
  moveRects(bottomRects, 'bottom', sx)
}

function collisionCheck() {
  topRects.forEach(topRect => objectCollider(player, topRect, aabbCollisionCheck, separate))
  bottomRects.forEach(bottomRect => objectCollider(player, bottomRect, aabbCollisionCheck, separate))
}

let previousTime
function gameLoop(currentTime) {
  requestAnimationFrame(gameLoop)
  if (!previousTime) {
    previousTime = currentTime
  }
  const dt = (currentTime - previousTime) * 0.1

  moveRects(topRects, 'top', player.horizontalSpeed * dt)
  moveRects(bottomRects, 'bottom', player.horizontalSpeed * dt)

  if (isJumping && player.verticalSpeed > 0 && !hasJumped) {
    player.verticalSpeed = -5
    hasJumped = true
  }
  else if (!isJumping) {
    hasJumped = false
  }

  applyVerticalSpeed(player, dt)
  applyVerticalAcceleration(player, 0.3, dt)
  collisionCheck()

  previousTime = currentTime
}

requestAnimationFrame(gameLoop)
